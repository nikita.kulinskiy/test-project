import React from 'react';
import {Layout} from 'antd';
import {Redirect, Route, Switch, useHistory} from 'react-router-dom';

import AntSider from '../layout/AntSider';
import AntHeader from '../layout/AntHeader';
import AntFooter from '../layout/AntFooter';
import useStateLogin from '../hooks/useStateLogin';

import './app.scss';
import useLoginActions from '../hooks/useLoginAction';
import Login from '../pages/Login';
import Users from '../pages/Users';
import Drivers from '../pages/Drivers';
import Error from '../pages/Error';

const App = () => {
    const {isLogin} = useLoginActions();
    const login = useStateLogin();
    const history = useHistory();

    if (localStorage.getItem('success')) {
        isLogin(true);
    }

    if (!login) {
        history.push('/login');
        return (
            <div className="app-layout">
                <Layout>
                    <Switch>
                        <Route exact path="/">
                            <Redirect to="/login" />
                        </Route>
                        <Route exact path="/login" component={Login} />
                        <Route component={Error} />
                    </Switch>
                </Layout>
            </div>
        );
    }
    return (
        <div className="app-layout">
            <Layout>
                <AntSider />
                <Layout className="site-layout">
                    <AntHeader />
                    <Switch>
                        <Route exact path="/">
                            <Redirect to="/users" />
                        </Route>
                        <Route exact path="/login">
                            <Redirect to="/users" />
                        </Route>
                        <Route exact path="/users" component={Users} />
                        <Route exact path="/drivers" component={Drivers} />
                        <Route component={Error} />
                    </Switch>
                    <AntFooter />
                </Layout>
            </Layout>
        </div>
    );
};

export default App;
