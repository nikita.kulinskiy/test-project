import {Layout, Button} from 'antd';
import React from 'react';

import './antheader.scss';
import useLoginActions from '../../hooks/useLoginAction';

const {Header} = Layout;

const AntHeader = () => {
    const {isLogin} = useLoginActions();
    const removeStorage = () => {
        localStorage.removeItem('success');
        isLogin(false);
    };
    return (
        <Header className="site-layout-background">
            <Button type="link" className="logout-button" onClick={removeStorage}>
                Logout
            </Button>
        </Header>
    );
};

export default AntHeader;
