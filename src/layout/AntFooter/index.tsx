import {Layout} from 'antd';
import React from 'react';

import './antfooter.scss';

const {Footer} = Layout;

const AntFooter = () => {
    return (
        <div className="custom-footer">
            <Footer>Company ©2021</Footer>
        </div>
    );
};

export default AntFooter;
