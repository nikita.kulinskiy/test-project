import {Layout, Menu} from 'antd';
import {DesktopOutlined, CarOutlined} from '@ant-design/icons';
import React, {useState} from 'react';
import {Link} from 'react-router-dom';

import './antsider.scss';

const {Sider} = Layout;

const AntSider = () => {
    const [collapsed, setCollapsed] = useState(false);
    return (
        <Sider collapsible collapsed={collapsed} onCollapse={setCollapsed}>
            <div className="logo">{collapsed ? 'C' : 'Company'}</div>
            <Menu theme="dark" mode="inline">
                <Menu.Item key="1" icon={<DesktopOutlined />}>
                    <Link to="/users">Users</Link>
                </Menu.Item>
                <Menu.Item key="2" icon={<CarOutlined />}>
                    <Link to="/drivers">Drivers</Link>
                </Menu.Item>
            </Menu>
        </Sider>
    );
};

export default AntSider;
