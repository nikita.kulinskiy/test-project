import {useDispatch} from 'react-redux';
import isLogin from '../store/actions';

export default function useLoginActions() {
    const dispatch = useDispatch();
    return {
        isLogin: (value: boolean) => dispatch(isLogin(value)),
    };
}
