import {useSelector} from 'react-redux';

export default function useStateLogin() {
    return useSelector((state: {isLogin: boolean}) => state.isLogin);
}
