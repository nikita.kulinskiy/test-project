import * as loaderTypes from '../actionsTypes';

export default function isLogin(value: boolean) {
    return (dispatch: any) => {
        dispatch({
            type: loaderTypes.IS_LOGIN,
            isLogin: value,
        });
    };
}
