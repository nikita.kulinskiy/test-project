import * as loaderTypes from '../actionsTypes';

const initialState = {
    isLogin: false,
};

export const reducer = (state = initialState, action: any) => {
    switch (action.type) {
        case loaderTypes.IS_LOGIN:
            return {
                ...state,
                isLogin: action.isLogin,
            };
        default:
            return state;
    }
};

export default reducer;
