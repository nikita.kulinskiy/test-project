const filterItems = (obj: Array<any>, value: string) => {
    const res: any = obj.filter((el: any) => {
        return el.surname.toLowerCase().indexOf(value.toLowerCase()) > -1;
    });
    return res;
};

export default filterItems;
