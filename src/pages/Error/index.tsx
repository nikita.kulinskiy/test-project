import React from 'react';

import './error.scss';

const Error = () => {
    return (
        <div className="site-layout-background error-layout">
            <h1 className="text__center">404</h1>
            <p className="text__center"> The page you were looking for doesn’t exist</p>
        </div>
    );
};

export default Error;
