import React, {useState} from 'react';
import {Table, Input} from 'antd';
import backEndData from '../../components/API/index.json';
import filterItems from '../../components/filterItems';

import './drivers.scss';

const Drivers = () => {
    const [filterDrivers, setFilterDrivers] = useState(backEndData.driver);
    const searchItems = (obj: Array<any>, value: string) => {
        const res = filterItems(obj, value);
        setFilterDrivers(res);
    };
    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Surname',
            dataIndex: 'surname',
            key: 'surname',
        },
        {
            title: 'Phone',
            dataIndex: 'phone',
            key: 'phone',
        },
    ];
    return (
        <>
            <div className="site-layout-background custom-drivers-layout">
                <h1>Drivers</h1>
                <Input
                    placeholder="Search"
                    onChange={(e) => {
                        searchItems(backEndData.driver, e.target.value);
                    }}
                />
                <br />
                <br />
                <Table dataSource={filterDrivers} columns={columns} />
            </div>
        </>
    );
};

export default Drivers;
