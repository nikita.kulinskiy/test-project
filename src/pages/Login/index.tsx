import React from 'react';
import {Form, Input, Button} from 'antd';

import BackEndData from '../../components/API/index.json';
import useLoginActions from '../../hooks/useLoginAction';

import './login.scss';

const Login = () => {
    const {isLogin} = useLoginActions();

    const layout = {
        labelCol: {span: 5},
        wrapperCol: {span: 18},
    };
    const tailLayout = {
        wrapperCol: {offset: 8, span: 16},
    };

    const onFinish = (values: any) => {
        const user: any = BackEndData.users.find(({email}) => email === values.email);
        // eslint-disable-next-line no-console
        console.log(user.email, values.email, user.pass, values.pass);
        if (user.email === values.email && Number(user.pass) === Number(values.pass)) {
            localStorage.setItem('success', 'true');
            isLogin(true);
        }
    };

    return (
        <div className="loading-block">
            <Form {...layout} name="basic" initialValues={{remember: true}} onFinish={onFinish}>
                <Form.Item label="Email" name="email" rules={[{required: true, message: 'Please input your email!'}]}>
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="pass"
                    rules={[{required: true, message: 'Please input your password!'}]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
};

export default Login;
