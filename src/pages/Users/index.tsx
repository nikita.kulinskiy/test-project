import React, {useState} from 'react';
import {Table, Input} from 'antd';
import backEndData from '../../components/API/index.json';
import filterItems from '../../components/filterItems';

import './users.scss';

const Users = () => {
    const [filterUsers, setFilterUsers] = useState(backEndData.users);
    const searchItems = (obj: Array<any>, value: string) => {
        const res = filterItems(obj, value);
        setFilterUsers(res);
    };

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Surname',
            dataIndex: 'surname',
            key: 'surname',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        },
    ];

    return (
        <>
            <div className="site-layout-background custom-users-layout">
                <h1>Users</h1>
                <Input
                    placeholder="Search"
                    onChange={(e) => {
                        searchItems(backEndData.users, e.target.value);
                    }}
                />
                <br />
                <br />
                <Table dataSource={filterUsers} columns={columns} />
            </div>
        </>
    );
};

export default Users;
